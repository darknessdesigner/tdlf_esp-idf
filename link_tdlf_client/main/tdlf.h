#ifndef TDLF_H_INCLUDED
#define TDLF_H_INCLUDED

#include <ableton/Link.hpp>
#include <driver/timer.h>
#include "nvs_flash.h"
///////// WiFI ///////
#include "esp_wifi.h"
#include "esp_smartconfig.h"
#include "freertos/event_groups.h"
///////// TOUCH //////
#include "driver/touch_pad.h"
#include "soc/rtc_periph.h"
#include "soc/sens_periph.h"
///////// OSC ///////
#include <tinyosc.h> // testing for speed !?
///////// MDNS //////
#include "mdns.h"
#include "netdb.h" 
#include "esp_netif.h"
#include "esp_netif_ip_addr.h"

#endif

extern "C" {
static const char *SOCKET_TAG = "Socket";
static const char *SMART_TAG = "Smart config";
static const char *NVS_TAG = "NVS";
static const char *WIFI_TAG = "Wifi";
static const char *TAG = "tdlf";
}

//#define HOST_IP_ADDR "255.255.255.255" // trying to broadcast first //
#define HOST_IP_ADDR "192.168.0.104" // hard coded //
esp_ip4_addr* MDNS_HOST_IP;
char mdns_host_ip_str[16];

#define PORT 3333

////// sockette /////
char rx_buffer[79]; // To hold incoming sequence
char addr_str[128];
int addr_family;    
int ip_protocol;
struct sockaddr_in dest_addr;
int sock;

bool bdChanged = false; // Was the sequence changed? Replace this with : 
bool sendData = false;  // Was the sequence changed? If so send the data over to the server

int press;
int selektor; // couleur rose par défaut
int modSelektor = 0;
int midiChannel = 10; // channel à stocker ds les premiers 0-3 bits de bd[]
int noteSelektor = 0; // note à stocker dans les bits 4-7 de bd[]
int noteDuration = 0; // length of note stored in bits 8-11 of bd[] 
int barSelektor = 0; // à stocker
int currentBar = 0; // pour l'affichage

/// OSC ///
uint8_t midi[4]; // This will hold the data to be sent over

/////// TIMER + button state//////
// https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/system/esp_timer.html?highlight=hardware%20timer High Resoultion Timer API

esp_timer_handle_t oneshot_timer;
bool origButtonState; // to store button value, restore to it if we had a long press
bool currTouch; // to store if button is currently touched
bool continuousPress = false;

typedef struct {
    bool on;          
    uint8_t chan;
    uint8_t bar;
    uint8_t note;
    uint8_t length;
    bool mute;
} steps_t;

steps_t steps[64]; // Declare an array of type struct steps

int step = 0;
float oldstep;

/////// SEQ ///////

bool startStopCB = false; // l'état du callback 
bool startStopState = false; // l'état local
bool isPlaying = true;
double curr_beat_time;
double prev_beat_time;

#include "leds.h"
#include "ableton_.cpp"
#include "touch.h"


#include "mdns_.h"
#include "socket_client.cpp"
#include "wifi_config.cpp"
#include "nvs_.cpp"
#include "leds.cpp"
