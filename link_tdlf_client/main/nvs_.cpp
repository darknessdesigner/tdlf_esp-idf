void nvsReadCredentials(){
    nvs_handle wificfg_nvs_handler; // Might need to revert to previous version
    size_t len;
    nvs_open("Wifi", NVS_READWRITE, &wificfg_nvs_handler);

    nvs_get_str(wificfg_nvs_handler, "wifi_ssid", NULL, &len);
    char* lssid = (char*)malloc(len); // (char*)malloc(len) compiles but crashes
    nvs_get_str(wificfg_nvs_handler, "wifi_ssid", lssid, &len);

    nvs_get_str(wificfg_nvs_handler, "wifi_password", NULL, &len);
    char* lpassword = (char*)malloc(len); // (char*)malloc(len) compiles but crashes
    nvs_get_str(wificfg_nvs_handler, "wifi_password", lpassword, &len); // esp_err_tnvs_get_str(nvs_handle_thandle, const char *key, char *out_value, size_t *length)

    nvs_close(wificfg_nvs_handler);
    
    ESP_LOGI(NVS_TAG,"INIT :%s",lssid); 
    ESP_LOGI(NVS_TAG,"INIT :%s",lpassword); 

}

void initNVS(){
//Initialize and read in wifi credentials from NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    
    /// Error check NVS credentials

    esp_err_t err;
	  size_t len;
	  // Open
	  nvs_handle wificfg_nvs_handler;
	  err = nvs_open("Wifi", NVS_READWRITE, &wificfg_nvs_handler);
        
      if (err != ESP_OK) {
          printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
          skipNVSRead = true;
      } else {
	      nvs_get_str(wificfg_nvs_handler, "wifi_ssid", NULL, &len);
        char* lssid = (char*)malloc(len); // (char*)malloc(len) compiles but crashes
        err = nvs_get_str(wificfg_nvs_handler, "wifi_ssid", lssid, &len);
	      switch (err) {
	        case ESP_OK:
	        break;
	        case ESP_ERR_NVS_NOT_FOUND:
	          printf("Key wifi_ssid is not initialized yet!\n");
            skipNVSRead = true;
	          break;
	        default :
	          printf("Error (%s) reading wifi_ssid size!\n", esp_err_to_name(err));
	          skipNVSRead = true;
            break;
	        }
        }
      nvs_close(wificfg_nvs_handler);

    if(skipNVSRead){
      //////// WRITING DUMMY VALUES TO NVS FROM CLEAN SHEET //////
      nvs_handle wificfg_nvs_handler;
      nvs_open("Wifi", NVS_READWRITE, &wificfg_nvs_handler);
      nvs_set_str(wificfg_nvs_handler,"wifi_ssid","testing");
      nvs_set_str(wificfg_nvs_handler,"wifi_password","onetwo");
      nvs_commit(wificfg_nvs_handler); 
      nvs_close(wificfg_nvs_handler); 
      ////// END NVS ///// 
    }
  nvsReadCredentials();
}

