
void initLEDs(){
    printf("\r\n\r\nHello Pixels!\n");
    //Set up SPI
    printf("Setting up SPI now\t[%d]\r\n", setupSPI());
    //set up LED object
    printf("Creating led object...\t");
    initLEDs(&leds, totalPixels, bytesPerPixel, 255); // 255
    printf("Frame Length\t%d\r\n", leds._frameLength);
    //set up colours
    initComplexColourObject(&dynColObject, maxValuePerColour, 9, colourList);	
    //Set up SPI tx/rx storage Object
    memset(&spiTransObject, 0, sizeof(spiTransObject));
    spiTransObject.length = leds._frameLength*8;
    spiTransObject.tx_buffer = leds.LEDs;
    printf("SPI Object Initialized...\r\n");
    printf("Sending SPI data block to clear all pixels....\r\n");
    spi_device_queue_trans(spi, &spiTransObject, portMAX_DELAY);
    printf("Pixels Cleared!\r\n");
}