// 15/02/2024
// tdlf client // 16 touch pads // 16 leds // envoi des données de un array au client
// LEDS driver from https://github.com/limitz/esp-apa102
// Based on the ESP32 Ableton Link port by Mathias Bredholt https://github.com/mathiasbredholt/link-esp

#include "tdlf.h"


static void oneshot_timer_callback(void* arg)
{
    //int64_t time_since_boot = esp_timer_get_time();
    //ESP_LOGI(TAG, "Short press, there has been no touch event for over 40 ms");
    
    if ( press < 20) { // we had a short touch after all
        steps[selektor+16*barSelektor].on = !steps[selektor+16*barSelektor].on; // essaie d'écrire...espero
        
        midi[0] = midiChannel;
        //midi[0] = 10;
        midi[1] = noteSelektor;
        midi[2] = steps[selektor+16*barSelektor].on;
        midi[3] = selektor+16*barSelektor; // which step is the note info on ?
        sendData = true;
        press = 0; // reset press

        /* for ( int i = 0 ; i < 64 ; i++ ) {
            ESP_LOGI(TAG, "step %i, value : %i", i, steps[i].on);
        } */
    }

    if ( continuousPress == true ){
        // ESP_LOGI(TAG, "> 200 ms Time to register the continous press and move on");
        continuousPress = false;
        press = 0;
    }

    currTouch = false; // reset so we know
}

extern "C" void app_main()
{
    initNVS();
    initLEDs();
    wifi_init_sta();
    //search for esp32-mdns.local
    //resolve_mdns_host("esp32-mdns");
    ESP_ERROR_CHECK( mdns_init() );
    resolve_mdns_host("tdlf-mdns");
    //find_mdns_service("_osc", "_udp");

    if (enableSmartConfig == true){
        //ESP_ERROR_CHECK(esp_wifi_stop());
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        //ESP_LOGI(WIFI_TAG, "STOP STA");
        xTaskCreate(smartconfig_example_task, "smartconfig_example_task", 4096, NULL, 3, &xHandle);
    }
    xTaskCreate(udp_client_task, "udp_client", 4096, NULL, 5, NULL);

    initTouch();

    // link  
    SemaphoreHandle_t tickSemphr = xSemaphoreCreateBinary();
    timerGroup0Init(1000, tickSemphr); // error: 'timerGroup0Init' was not declared in this scope
    xTaskCreate(tickTask, "tick", 8192, tickSemphr, 1, nullptr); // : error: 'timerGroup0Init' was not declared in this scope
	
    const esp_timer_create_args_t oneshot_timer_args = {
        .callback = &oneshot_timer_callback,
        // argument specified here will be passed to timer callback function 
       .arg = NULL,
        //.dispatch_method = ESP_TIMER_TASK,
        .name = "one-shot"
    };

    esp_timer_create(&oneshot_timer_args, &oneshot_timer);
    //esp_timer_start_once(oneshot_timer, 5000000);
    // end timers ?

     vTaskDelete(nullptr);

} // fin du extern "C"