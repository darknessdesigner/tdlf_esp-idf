// ableton utility functions

void IRAM_ATTR timer_group0_isr(void* userParam)
{
  static BaseType_t xHigherPriorityTaskWoken = pdFALSE;
  TIMERG0.int_clr_timers.t0 = 1;
  TIMERG0.hw_timer[0].config.alarm_en = 1;

  xSemaphoreGiveFromISR(userParam, &xHigherPriorityTaskWoken);
  if (xHigherPriorityTaskWoken)
  {
    portYIELD_FROM_ISR();
  }
}

void timerGroup0Init(int timerPeriodUS, void* userParam)
{
  timer_config_t config = {.alarm_en = TIMER_ALARM_EN,
    .counter_en = TIMER_PAUSE,
    .intr_type = TIMER_INTR_LEVEL,
    .counter_dir = TIMER_COUNT_UP,
    .auto_reload = TIMER_AUTORELOAD_EN,
    .divider = 80};

  timer_init(TIMER_GROUP_0, TIMER_0, &config);
  timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0);
  timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, timerPeriodUS);
  timer_enable_intr(TIMER_GROUP_0, TIMER_0);
  timer_isr_register(TIMER_GROUP_0, TIMER_0, &timer_group0_isr, userParam, 0, nullptr);

  timer_start(TIMER_GROUP_0, TIMER_0);
}


void startStopChanged(bool state) {   // received as soon as sent, we can get the state of 'isPlaying' and use that
  startStopCB = state;  // need to wait for phase to be 0 (and deal with latency...)
  ESP_LOGI(TAG, "StartStopCB : %d", startStopCB); 
}


void tickTask(void* userParam)
{
    ESP_LOGI(TAG, "link task started");

    // connect link
    ableton::Link link(120.0f);
    link.enable(true);
    link.enableStartStopSync(true); // if not no callback for start/stop
    // ESP_LOGI(TAG, "link enabled ! ");
    
    // callbacks
    // link.setTempoCallback(tempoChanged);
    link.setStartStopCallback(startStopChanged);
    
    while (true)
    { // while (true)
        xSemaphoreTake((QueueHandle_t)userParam, portMAX_DELAY);

        const auto state = link.captureAudioSessionState();
        isPlaying = state.isPlaying();
        //  ESP_LOGI(TAG, "isPlaying : , %i", isPlaying);  
        curr_beat_time = state.beatAtTime(link.clock().micros(), 4); 
        const double curr_phase = fmod(curr_beat_time, 4); 

        if (curr_beat_time > prev_beat_time && isPlaying) {

            const double prev_phase = fmod(prev_beat_time, 4);
            const double prev_step = floor(prev_phase * 4);
            const double curr_step = floor(curr_phase * 4);
            
            // https://github.com/libpd/abl_link/blob/930e8c0781b8afe9fc68321fe64c32d6e92dc113/external/abl_link~.cpp#L84
            if (abs(curr_step - prev_step) > 4 / 2 || prev_step != curr_step) {  // quantum divisé par 2

                //ESP_LOGI(TAG, "curr_step : , %f", curr_step);  
                //ESP_LOGI(TAG, "step : , %i", step);  
                //ESP_LOGI(TAG, " ");  
                step = int(curr_step);
                //ESP_LOGI(TAG, "step : , %i", step);  
                //ESP_LOGI(TAG, " ");  

                if( curr_step == 0 && isPlaying ){ 
                  
                   if(startStopCB) { // on recommence à zéro si on a reçu un message de départ
                        step = 0; // reset le compteur
                        currentBar = 0; // reset du bar
                        startStopCB = !startStopCB;
                        }    
                }   

                if(isPlaying){
                    TurnLedOn(step); // allumer les DELs 
                }
                
                if (step == 15){ //
                    if( currentBar < barSelektor ){ 
                        currentBar = currentBar + 1;
                        }
                    else{
                        currentBar = 0;
                    }
                }

            }    
        }

    prev_beat_time = curr_beat_time;

    //portYIELD();  
    vTaskDelay(20 / portTICK_PERIOD_MS); // 2 // 5 // 10 // 20 

    } // fin du while true

} // fin de tickTask

