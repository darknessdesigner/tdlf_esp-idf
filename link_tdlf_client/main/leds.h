///////// DELS // SPI CONFIG TTGO // VSPI // SPI3 // MOSI 23 // SCK 18
extern "C"{
#include "ESP32APA102Driver.h"

unsigned char colourList[9*3]={maxValuePerColour,0,0, maxValuePerColour,maxValuePerColour,0, 0,maxValuePerColour,0, 0,maxValuePerColour,maxValuePerColour, 0,0,maxValuePerColour, maxValuePerColour,0,maxValuePerColour, maxValuePerColour,maxValuePerColour,maxValuePerColour, maxValuePerColour,0,0, 0,0,0};

struct apa102LEDStrip leds;
struct colourObject dynColObject;

// SPI Vars
spi_device_handle_t spi;
spi_transaction_t spiTransObject;
esp_err_t ret;
spi_bus_config_t buscfg;
spi_device_interface_config_t devcfg;

unsigned char Colour[8][3] = { {14,5,7},{13,0,0},{14,6,0},{14,14,0},{0,5,0},{0,7,13},{3,0,9},{9,0,9}}; // sur 16 // rose // rouge // orange // jaune // vert // bleu clair // bleu foncé // mauve
unsigned char beatStepColour[3] = {13,13,5};
unsigned char stepColour[3] = {13,8,13};
unsigned char offColour[3] = {0,0,0};
unsigned char inColour[3] = {14,14,14}; // init indicator (wifi, broadcast, server address received, ready to go)
unsigned char selektorColour[10][3] = {{6,6,6},{7,7,7},{8,8,8},{9,9,9},{10,10,10},{11,11,11},{12,12,12},{13,13,13},{14,14,14},{15,15,15}}; // for feedback until knowing if it is a short or long touch
}

extern "C"{
void renderLEDs()
{
	spi_device_queue_trans(spi, &spiTransObject, portMAX_DELAY);
}

int setupSPI()
{
	//Set up the Bus Config struct
	buscfg.miso_io_num=-1;
	buscfg.mosi_io_num=PIN_NUM_MOSI;
	buscfg.sclk_io_num=PIN_NUM_CLK;
	buscfg.quadwp_io_num=-1;
	buscfg.quadhd_io_num=-1;
	buscfg.max_transfer_sz=maxSPIFrameInBytes;
	
	//Set up the SPI Device Configuration Struct
	devcfg.clock_speed_hz=maxSPIFrequency;
	devcfg.mode=0;                        
	devcfg.spics_io_num=-1;             
	devcfg.queue_size=1;

	//Initialize the SPI driver
	ret=spi_bus_initialize(VSPI_HOST, &buscfg, 1);
    ESP_ERROR_CHECK(ret);	
	//Add SPI port to bus
	ret=spi_bus_add_device(VSPI_HOST, &devcfg, &spi);
	ESP_ERROR_CHECK(ret);
	return ret;
}

void LEDinDicator(int lvl){
    
    for(int i = 0;i<lvl;i++){
        setPixel(&leds, i, inColour); // plus clair
	}
    renderLEDs();
	vTaskDelay(50 / portTICK_PERIOD_MS); // 10
}


void TurnLedOn(int step){  //ESP32APA102Driver

        // ESP_LOGI(TAG, "step : %i",step);
 
    	for (int i = 0; i < 16; i++){ 
			setPixel(&leds, i, offColour); // turn LED off
            if(i == step && currentBar == barSelektor){ // sélecteur d'affichage de bar
                if (steps[i+16*barSelektor].on){
                    setPixel(&leds, i, beatStepColour);    
                    }else{
                    setPixel(&leds, i, stepColour); 
                    }
                } else if (steps[i+16*barSelektor].on){
                setPixel(&leds, i, Colour[noteSelektor]);           
                }
		}
        if ( currTouch ) {
            int instaCol = int(press/2); // press has 20 values max
            setPixel(&leds, selektor, selektorColour[instaCol]); // indicate button touch state, short or continuous long we don't know yet
        }

        if ( continuousPress ) {
            setPixel(&leds, selektor, Colour[6]);
        }
        renderLEDs();
	//vTaskDelay(1 / portTICK_PERIOD_MS); // 2 better //10
    }
}

