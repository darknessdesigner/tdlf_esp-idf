extern "C"{

//static void  query_mdns_host_with_gethostbyname(char *host);
//static void  query_mdns_host_with_getaddrinfo(char *host);

    void resolve_mdns_host(const char * host_name)
    {
        // vprintf("Query A: %s.local", host_name);

        struct esp_ip4_addr addr; // changed from 'struct ip4_addr addr;'
        // Because I had this : //cannot convert 'ip4_addr*' to 'esp_ip4_addr_t*' {aka 'esp_ip4_addr*'}
        addr.addr = 0;

        esp_err_t err = mdns_query_a(host_name, 10000,  &addr); 
        //vTaskDelay(10000 / portTICK_PERIOD_MS); 
        if(err){
            
            char errorMessage[24];
            printf(esp_err_to_name_r(err, errorMessage, sizeof(errorMessage)));

            if(err == ESP_ERR_NOT_FOUND){
                printf("Host was not found! %i", err);
                //ESP_LOGI(SOCKET_TAG, "tdlfServerIP : %s", rx_buffer);
                //return;
            }
            printf("Query Failed %i", err);
            //return;
        }
        else{
        // printf(IPSTR, IP2STR(&addr));
        MDNS_HOST_IP = &addr;
        esp_ip4addr_ntoa(MDNS_HOST_IP, mdns_host_ip_str, sizeof(mdns_host_ip_str));
        //ESP_LOGI(SOCKET_TAG, "mdns host IP address : %i.%i.%i.%i", IP2STR(MDNS_HOST_IP));
        ESP_LOGI(SOCKET_TAG, "mdns host IP address : %s\n", mdns_host_ip_str);
        
        //ESP_LOGI(WIFI_TAG, "modSelektor %d",);
        }
    }

/*void mdns_print_results(mdns_result_t * results){
    mdns_result_t * r = results;
    mdns_ip_addr_t * a = NULL;
    int i = 1, t;
    while(r){
        printf("%d: Interface: %s, Type: %s\n", i++, if_str[r->tcpip_if], ip_protocol_str[r->ip_protocol]);
        if(r->instance_name){
            printf("  PTR : %s\n", r->instance_name);
        }
        if(r->hostname){
            printf("  SRV : %s.local:%u\n", r->hostname, r->port);
        }
        if(r->txt_count){
            printf("  TXT : [%u] ", r->txt_count);
            for(t=0; t<r->txt_count; t++){
                printf("%s=%s; ", r->txt[t].key, r->txt[t].value);
            }
            printf("\n");
        }
        a = r->addr;
        while(a){
            if(a->addr.type == IPADDR_TYPE_V6){
                printf("  AAAA: " IPV6STR "\n", IPV62STR(a->addr.u_addr.ip6));
            } else {
                printf("  A   : " IPSTR "\n", IP2STR(&(a->addr.u_addr.ip4)));
            }
            a = a->next;
        }
        r = r->next;
    }

}


    void find_mdns_service(const char * service_name, const char * proto)
{
    ESP_LOGI(TAG, "Query PTR: %s.%s.local", service_name, proto);

    mdns_result_t * results = NULL;
    esp_err_t err = mdns_query_ptr(service_name, proto, 3000, 20,  &results);
    vTaskDelay(10000 / portTICK_PERIOD_MS); 
    if(err){
        ESP_LOGE(TAG, "Query Failed");
        return;
    }
    if(!results){
        ESP_LOGW(TAG, "No results found!");
        return;
    }

    mdns_print_results(results);
    //mdns_query_results_free(results);
}
*/
} // End Extern "C"