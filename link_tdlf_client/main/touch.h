//// TOUCH ////
// ESP32 NODEMCU Touch pins 0_P4,2_P2,3_P15,4_P13,5_P12,6_P14,7_P27,9_P32
#define TOUCH_THRESH_NO_USE   (0)
#define TOUCH_THRESH_PERCENT  (90)
#define TOUCHPAD_FILTER_TOUCH_PERIOD (10)

static bool s_pad_activated[TOUCH_PAD_MAX];
static uint32_t s_pad_init_val[TOUCH_PAD_MAX];

int interval = 0; 
int oldTime = 0;

//extern "C" {
////////// TOUCH ///////////
static void tp_example_set_thresholds(void)
{
    uint16_t touch_value;
    for (int i = 0; i < TOUCH_PAD_MAX; i++) {
        //read filtered value
        touch_pad_read_filtered((touch_pad_t)i, &touch_value);
        s_pad_init_val[i] = touch_value;
        ESP_LOGI(TAG, "Init: touch pad [%d] val is %d", i, touch_value);
        //set interrupt threshold.
        ESP_ERROR_CHECK(touch_pad_set_thresh((touch_pad_t)i, touch_value * 2 / 3));
    }
}

static void tp_example_rtc_intr(void *arg)
{
    uint32_t pad_intr = touch_pad_get_status();
    //clear interrupt
    touch_pad_clear_status();
    for (int i = 0; i < TOUCH_PAD_MAX; i++) {
        if ((pad_intr >> i) & 0x01) {
            s_pad_activated[i] = true;
        }
    }
} 

static void tp_example_touch_pad_init(void) // Before reading touch pad, we need to initialize the RTC IO.
{
    for (int i = 0; i < TOUCH_PAD_MAX; i++) {
        //init RTC IO and mode for touch pad.
        touch_pad_config((touch_pad_t)i, TOUCH_THRESH_NO_USE);
    }
}

static void tp_example_read_task(void *pvParameter)
{
    while (1) {
       
        touch_pad_intr_enable(); //interrupt mode, enable touch interrupt
            
        for (int i = 0; i < TOUCH_PAD_MAX; i++) {

            if (s_pad_activated[i] == true) {

                interval = (esp_timer_get_time() - oldTime)/1000;  // measure time between button events
                // ESP_LOGI(SPACE_TAG, " ");
                // ESP_LOGI(TAG, "Interval, %i ms", interval);
                oldTime = esp_timer_get_time();

                esp_timer_stop(oneshot_timer); // stop it if we are here 
                esp_timer_start_once(oneshot_timer, 40000); // if this triggers, we confirmed we had a short press
                
                // sendData is determined if we have a new note in 'oneshot_timer_callback'

                if ( interval > 350 ) {
                    press = 0;
                }
                press++; 
                // ESP_LOGI(TAG, "press : %d",press);
                
                currTouch = true; // For LED indication

                if(s_pad_activated[2] && s_pad_activated[4]){ // P2 P13
                    ESP_LOGI(TAG, "piton 1");
                    selektor = 0;
                    //origButtonState = bd[15+selektor+16*barSelektor]; // store value // capture piton state and return to original state if it was a long press
                    origButtonState = steps[selektor+16*barSelektor].on; // store value
                    }
                if(s_pad_activated[0] && s_pad_activated[4]){ // P4 P13
                    ESP_LOGI(TAG, "piton 2");
                    selektor = 1;
                    origButtonState = steps[selektor+16*barSelektor].on; // store value
                    }
                if(s_pad_activated[3] && s_pad_activated[4]){ // P15 P13
                    ESP_LOGI(TAG, "piton 3");
                    selektor = 2;
                    origButtonState = steps[selektor+16*barSelektor].on; // store value
                    } 
                if(s_pad_activated[9] && s_pad_activated[4]){ // P32 P13
                    ESP_LOGI(TAG, "piton 4");
                    selektor = 3;
                    origButtonState = steps[selektor+16*barSelektor].on; // store value
                    } 
                /////// 5-8
                if(s_pad_activated[2] && s_pad_activated[5]){ // P2 P12
                    ESP_LOGI(TAG, "piton 5");
                    selektor = 4;
                    origButtonState = steps[selektor+16*barSelektor].on; // store value
                    }
                if(s_pad_activated[0] && s_pad_activated[5]){ // P4 P12
                    ESP_LOGI(TAG, "piton 6");
                    selektor = 5;
                    origButtonState = steps[selektor+16*barSelektor].on; // store value
                    } 
                if(s_pad_activated[3] && s_pad_activated[5]){ // P15 P12
                    ESP_LOGI(TAG, "piton 7");
                    selektor = 6;
                    origButtonState = steps[selektor+16*barSelektor].on; // store value
                    } 
                if(s_pad_activated[9] && s_pad_activated[5]){ // P32 P12
                    ESP_LOGI(TAG, "piton 8");
                    selektor = 7;
                    origButtonState = steps[selektor+16*barSelektor].on; // store value
                    } 
                /////// 9-12
                if(s_pad_activated[2] && s_pad_activated[6]){ // P2 P14
                    ESP_LOGI(TAG, "piton 9");
                    selektor = 8;
                    origButtonState = steps[selektor+16*barSelektor].on; // store value
                    }
                if(s_pad_activated[0] && s_pad_activated[6]){ // P4 P14
                    ESP_LOGI(TAG, "piton 10"); 
                    selektor = 9;
                    origButtonState = steps[selektor+16*barSelektor].on; // store value
                    }
                if(s_pad_activated[3] && s_pad_activated[6]){ // P15 P14
                    ESP_LOGI(TAG, "piton 11");
                    selektor = 10;
                    origButtonState = steps[selektor+16*barSelektor].on; // store value
                    }
                if(s_pad_activated[9] && s_pad_activated[6]){ // P32 P14
                    ESP_LOGI(TAG, "piton 12");
                    selektor = 11;
                    origButtonState = steps[selektor+16*barSelektor].on; // store value
                    }
                /////// 13-16
                if(s_pad_activated[2] && s_pad_activated[7]){ // P2 P27
                    ESP_LOGI(TAG, "piton 13");
                    selektor = 12;
                    origButtonState = steps[selektor+16*barSelektor].on; // store value
                    }
                if(s_pad_activated[0] && s_pad_activated[7]){ // P4 P27
                    ESP_LOGI(TAG, "piton 14");
                    selektor = 13;
                    origButtonState = steps[selektor+16*barSelektor].on; // store value
                    }
                if(s_pad_activated[3] && s_pad_activated[7]){ // P15 P27
                    ESP_LOGI(TAG, "piton 15");
                    selektor = 14;
                    origButtonState = steps[selektor+16*barSelektor].on; // store value
                    }
                if(s_pad_activated[9] && s_pad_activated[7]){ // P32 P27
                    ESP_LOGI(TAG, "piton 16");
                    selektor = 15;
                    origButtonState = steps[selektor+16*barSelektor].on; // store value
                    }

                if(press == 20){ // number of consecutive 'presses' required for a long continuous press
                        
                        // okay we did it, feedback to the user and start a counter to prevent touches registering
                        // some bool to turn the led blue
                        
                        esp_timer_stop(oneshot_timer); // stop it if we are here 
                        continuousPress = true;
                        esp_timer_start_once(oneshot_timer, 200000); // delay after which we can reset continuous press
                        
                        ESP_LOGI(TAG, "stop the timer!!!");

                        // press = 0; // reset press

                        modSelektor = selektor;
                        ESP_LOGI(TAG, "modSelektor %d",modSelektor);
                        ESP_LOGI(TAG, " ");

                        if(modSelektor<8){ // change la valeur de note représentée par sa couleur
                          noteSelektor = modSelektor;  
                          ESP_LOGI(TAG, "noteSelektor %d", noteSelektor);
                          steps[0].note = noteSelektor; // Write the note value to the steps[] // Will need to write it in the substructure for up to 8 notes at the same time.
                          ESP_LOGI(TAG, "noteSelektor %i", steps[0].note);
                        }

                        else if(modSelektor>=8 && modSelektor<12){ // change le bar (1-16)(17-32)etc.
                           
                            // Need to know only once how many bars we have in the sequence
                            // *** Disabling the bar selektor :/ Too confusing for the space ***
                            // barSelektor = modSelektor-8;
                            barSelektor = 0;
                            // steps[0].bar = barSelektor;
                            steps[0].bar = 0;
                            //sendData = true;
                            //midi[0] = 17; // Code for a change in the bar number...changing the number of steps in the sequence (1-16)(17-32) // Implementing a smaller number of steps would be fun!
                            //midi[1] = 66;
                            //midi[2] = 66;
                            //midi[3] = 66;

                            ESP_LOGI(TAG, "Bar : %d", barSelektor);
                        }

                        else if( modSelektor == 12){ 
                            if (midiChannel == 7){ // le max
                                midiChannel = -1; // loop it
                                }
                            // *** Disabling midi channel for Banshees, too confusing, notes are sufficient for the solenoids.
                            // midiChannel = midiChannel+1 ; // à considérer un 'enter' pour rendre les modifs actives à ce moment uniquement
                            // Need to set the midi channel only once for the sequence
                            //steps[0].chan = midiChannel; // Write the channel number 
                            ESP_LOGI(TAG, "Midi channel %i", midiChannel);
                        }
                        
                        else if( modSelektor == 13 ){ // Won't change note duration for Banshees
                            if (noteDuration == 7) {
                                noteDuration = -1;
                            }
                            noteDuration = noteDuration + 1;
                            // Need to set the note length only once for the sequence
                            steps[0].length = noteDuration;
                            ESP_LOGI(TAG, "noteDuration : %i", steps[0].length); 
                        }

                        else if( modSelektor == 14 ) {
                            // Need to set the mute info only once for the sequence // Are we playing or not?
                            steps[0].mute = !steps[0].mute;
                            midi[0] = 18; // Channel (up to 16) so 18 is code for muting that track
                            midi[1] = 66;
                            midi[2] = 66;
                            midi[3] = 66;
                            sendData = true;
                            ESP_LOGI(TAG, "mute : %i", steps[0].mute); 
                        }
                        
                        else if( modSelektor == 15 ) { // reset values
                            // Erase all values from the sequencer
                            for (i = 0 ; i < 64 ; i++) {
                                steps[i].on = 0;
                                steps[i].chan = 0;
                                // steps[i].note = 0;
                                steps[i].length = 0;
                                steps[i].mute = 0;
                            }

                            midi[0] = 19; // Code for reset on the server
                            midi[1] = 66; // no such note
                            midi[2] = 66; // no such bar
                            midi[3] = 66; 
                            sendData = true; // TODO : Ajouter l'envoi d'un message de reset du côté de tdlf // We need to send a reset message to the server
                            modSelektor = 42; // ok on arrête d'effacer...
                            ESP_LOGI(TAG, "RESET");
                        }

                    }

                    vTaskDelay(20 / portTICK_PERIOD_MS); // 100 Wait a while for the pad being released
                    s_pad_activated[i] = false; // Clear information on pad activation
                    // show_message = 1;  /// // Reset the counter triggering a message that application is running
                }
            }

        vTaskDelay(10 / portTICK_PERIOD_MS); // 10 since 5 triggers watchdog

    }
}

void initTouch(){
    /////// TOUCH INIT ////////
	ESP_LOGI(TAG, "Initializing touch pad");
	touch_pad_init(); // Initialize touch pad peripheral, it will start a timer to run a filter
	touch_pad_set_fsm_mode(TOUCH_FSM_MODE_TIMER); // If use interrupt trigger mode, should set touch sensor FSM mode at 'TOUCH_FSM_MODE_TIMER'.
	touch_pad_set_voltage(TOUCH_HVOLT_2V7, TOUCH_LVOLT_0V5, TOUCH_HVOLT_ATTEN_1V); // Set reference voltage for charging/discharging // For most usage scenarios, we recommend using the following combination: // the high reference valtage will be 2.7V - 1V = 1.7V, The low reference voltage will be 0.5V.
	tp_example_touch_pad_init(); // Init touch pad IO
	touch_pad_filter_start(TOUCHPAD_FILTER_TOUCH_PERIOD); // Initialize and start a software filter to detect slight change of capacitance.
	tp_example_set_thresholds(); // Set thresh hold
	touch_pad_isr_register(tp_example_rtc_intr, NULL); // Register touch interrupt ISR

	////////// TOUCH TASK //////////// // Start a task to show what pads have been touched
	xTaskCreate(&tp_example_read_task, "touch_pad_read_task", 2048, NULL, 5, NULL); // n'importe quelle core
	///////// FIN TOUCH /////////

    // link + udp task are started within the event handler as we need to wait for IP_EVENT_STA_GOT_IP
}
