////////// UDP SOCKETTE ////////
namespace {
    bool nouvSockette = false;
}

static void udp_client_task(void *pvParameters)
{

    LEDinDicator(8);

   
    while (1) { // sendData // bdChanged est un flag si bd[] a changé
        //
        /* for(int i = 0; i<sizeof(bd); i++){
            if (bd[i] != cbd[i]){
            ESP_LOGI(SOCKET_TAG, "bd changed 1!");
            bdChanged = true;
            break; 
            }
        } */

        if (!nouvSockette){

	        ESP_LOGI(SOCKET_TAG, "Trying to open a socket");
            ESP_LOGI(SOCKET_TAG, "Again, mdns host IP address : %s\n", mdns_host_ip_str);
            
            //dest_addr.sin_addr.s_addr = inet_addr(HOST_IP_ADDR); // HOST_IP_ADDR is hardcoded
            //dest_addr.sin_addr.s_addr = inet_addr(MDNS_HOST_IP); 
            //dest_addr.sin_addr.s_addr = inet_addr(mdns_host_ip_str); 
	     
            sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP); // Create UDP socket
            
            if (sock < 0) {
    	        ESP_LOGE(SOCKET_TAG, "Unable to create socket: errno %d", errno);
  		    } else {
                ESP_LOGI(SOCKET_TAG, "La Socket created");
            }
        ///// send init Message to the server /////
        memset(&dest_addr, 0, sizeof(dest_addr)); // memset needs to be in main
        dest_addr.sin_family = AF_INET;
        dest_addr.sin_port = htons(PORT);
        inet_pton(AF_INET, HOST_IP_ADDR , &dest_addr.sin_addr);

        const char *message = "Hello, UDP!";
        int err = sendto(sock, message, strlen(message), 0, (struct sockaddr *)&dest_addr, sizeof(dest_addr));
            if (err < 0) {
                ESP_LOGE(TAG, "Error sending UDP message: %d", errno);
                printf("err = %i\n", err);
                printf("%s", esp_err_to_name(err));
                printf("\n");
                }

        //////////////
        ESP_LOGE(SOCKET_TAG, "socket number :%d", sock);


  	        //ESP_LOGI(SOCKET_TAG, "La Socket created, sending to %s:%d", rx_buffer, PORT);
            //ESP_LOGI(SOCKET_TAG, "La Socket created, sending to %i.%i.%i.%i:%d", IP2STR(MDNS_HOST_IP), PORT);


            nouvSockette = true;
            LEDinDicator(16);
            

        } // fin de !nouvSockette

        if ( nouvSockette ) {

            //ESP_LOGI(SOCKET_TAG, "we have a new socket and the IP of the server to send to");

            if ( sendData ) { // This results from a new note being entered from the sequencer //
                // Testing sending OSC messages at the start
                char monBuffer[16]; // monBuffer[16] // // declare a buffer for writing the OSC packet into
                uint8_t midi[4];
                midi[0] = 4;   // midi channel // 90 + midi channel (note on)
                midi[1] = 0;   // midi note // BD // SN // LT // HT // CH // HH // CLAP // AG //
                midi[2] = 0;   // Control Change message (11 is expression) // Pitch (note value)
                midi[3] = 0;    // Extra                                         

                int maLen = tosc_writeMessage(
                    monBuffer, sizeof(monBuffer),
                    "/midi", // the address
                    //"#midi", // the address
                    "m",   // the format; 'f':32-bit float, 's':ascii string, 'i':32-bit integer
                    midi);

                int err = sendto(sock, monBuffer, maLen, 0,(struct sockaddr *)&dest_addr, sizeof(dest_addr));
   
                if (err < 0) {
                    ESP_LOGE(SOCKET_TAG, "Error occurred during sending: errno %d", errno);
                    break;
                } 
    
                ESP_LOGI(SOCKET_TAG, "Message sent again");

                // This is for the future : loading sequences from the server onto the clients
                /* struct sockaddr_in6 source_addr; // Large enough for both IPv4 or IPv6
                socklen_t socklen = sizeof(source_addr);
                //int len = recvfrom(sock, rx_buffer, sizeof(rx_buffer) - 1, 0, (struct sockaddr *)&source_addr, &socklen);
                int len = recvfrom(sock, rx_buffer, sizeof(rx_buffer), 0, (struct sockaddr *)&source_addr, &socklen);

                // Error occurred during receiving
                if (len < 0) {
                    ESP_LOGE(SOCKET_TAG, "recvfrom failed: errno %d", errno);
                    break;
                } else { // Data received
                    rx_buffer[len] = 0; // Null-terminate whatever we received and treat like a string
                
                    ESP_LOGI(SOCKET_TAG, "Received mstr ? %d bytes from %s:", len, addr_str);
                    
                    if ( len > 20 ){  // So we're getting love and an array to load
                            
                        for ( int i = 0; i < sizeof(rx_buffer); i++ ) {
                            // ESP_LOGI(SOCKET_TAG, "rx_buffer[%i] = %i ", i, rx_buffer[i]); 
                            bd[i] = rx_buffer[i]; // copy into bd[]
                            cbd[i] = rx_buffer[i]; // copy into cbd[] so the comparison is not false
                            ESP_LOGI(SOCKET_TAG, "rx_buffer -> bd[%i] = %i ", i, bd[i]);    
                        }
                        for ( int i = 0 ; i < 16 ; i++ ) {  // Add a visual of the new sequence!! Outside of the start/stop sequence
                            TurnLedOn(i);
                        }

                    }
                    
                    if(rx_buffer[0] == '1') { // looper number (exclusive so managed here)
                        // ESP_LOGI(SOCKET_TAG, "succès UDP");
                    }  */


                // vTaskDelay(10 / portTICK_PERIOD_MS); // 500
                
                sendData = false; // The latest change was sent

                } // Fin de 'did we have to sendData'?

                vTaskDelay(10 / portTICK_PERIOD_MS); // 500

        }  // fin mstrPuckIP && we have a nouvelle sockette 
        
    } // fin du while

    if (sock != -1) {
        ESP_LOGE(TAG, "Shutting down socket and restarting...");
        shutdown(sock, 0);
        close(sock);
    }
  
    vTaskDelete(NULL); 
} // fin extern "C"

