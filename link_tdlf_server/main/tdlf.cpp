#include "tdlf.h"

void initTDLF(){
    dest_addr.sin_family = AF_INET;
    dest_addr.sin_addr.s_addr = inet_addr(OSC_BROADCAST_ADDR); //  "192.168.0.255"
    dest_addr.sin_addr.s_addr = htonl(INADDR_ANY); // Bind to any available network interface
    dest_addr.sin_port = htons(OSC_BROADCAST_PORT);
    inet_pton(AF_INET, OSC_BROADCAST_ADDR, &dest_addr.sin_addr);
}

bool changedMstr = false;
float duration[8] = {0.25,0.5,1,2,4,8,16,64}; // 4 bits note duration (0-7) -> (64,16,8,4,2,1,1/2,1/4)
float noteDuration; 
int myNoteDuration;
int bar[8] = {1,1,1,1,1,1,1,1}; // 2 bits, up to 4 bars?
int myBar = 0; 
bool muteRecords[8] = {0,0,0,0,0,0,0,0}; // mute info per note
int stepsLength[4] = {16,32,48,64}; // varies per note 16-64

int beat = 0; 
int step = 0 ;
int dubStep = 0;
float oldstep;
int currStep = 0;

esp_timer_handle_t periodic_timer;

bool isPlaying = true;
bool changePiton = false; 
bool changeLink = false;
bool tempoINC = false; // si le tempo doit être augmenté
bool tempoDEC = false; // si le tempo doit être réduit
double newBPM; // send to setTempo();
double oldBPM; 
double curr_beat_time;
double prev_beat_time;

bool connektMode = true; // flag pour envoyer l'adresse IP aux clients
char str_ip[16] =  "192.168.0.66"; // send IP to clients !! // stand in ip necessary for memory space?

int nmbrSeq = 0; // the number of sequences in nvs
char sequence[8][10] = {{"sequence"},{"seq1"},{"seq2"},{"seq3"},{"seq4"},{"seq5"},{"seq6"},{"seq7"}};

///////////// TAP TEMPO //////////////
/// from : https://github.com/DieterVDW/arduino-midi-clock/blob/master/MIDI-Clock.ino

int foisTapped = 0;
bool toTapped = false;
int tapped = 0;
int lastTapTime = 0;
int tapInterval = 0;
int tapArray[5] = {0};
int tapTotal = 0;
int bpm;  // BPM in tenths of a BPM!!
int tappedBPM = 0;
int minimumTapInterval = 50;
int maximumTapInterval = 1500;

int test = 777;
int channel; // 4 bits midi channel (0-15) -> // drums + más
int note; // 4 bits note info // 8 notes correspond to 8 colors // (0-7) -> (36,38,43,50,42,46,39,75),67,49 // más de 8 !
int mtmss[1264] = {0}; // 79 x 16 géant et flat (save this for retrieval, add button to select and load them)
int CCChannel = 1;
int sensorValue = -42;
int sensorValue1 = 0;
int previousSensorValue = 0;

bool startStopCB = false; // l'état du callback 
bool startStopState = false; // l'état local

int nmbrClients = 0;
int loadedClients = 0;

///// sequences /////
bool loadedSeq[1264] = {}; // to store the loaded sequences
bool mstr[79] = {}; // mstr[0] (type of source) // mstr[1] (channel) // mstr[2] (note) // mstr[3] (note duration) // mstr[4] (bar) // mstr[5] (mute) // mstr[6] (erasePart) // mstr[7] (eraseAll) // mstr[15-79](steps)
bool oldmstr[79] = {1}; 

///////////// INTERACTIONS ///////////
/// save (Touch pad 2)
bool tapeArch = false; // flag for saving
bool saveBPM = false; 
bool saveSeq = false; // nvs save the sequence to start
bool seqSaved = false;
bool saveSeqConf = false;
bool loadSeq = false;
bool seqLoaded = false;
bool seqToLoad = false; // to know when to send the loaded sequence to clients
bool loadSeqConf = false;
bool saveDelay = false; // for when to remove the save options after an interaction
int delset = 0;
int selectedSeq = 0; // user selected sequence to load