#ifndef TDLF_H
#define TDLF_H

#include "ledc.h"
#include "display.h"
#include "midi_cv.h"
#include "socket_server.h"
#include "touch.h"
#include "wifi_mdns.h"
//#include "nvs_.cpp"
//#include "ableton_.cpp"



#include <ableton/Link.hpp>
#include <driver/timer.h>
#include <nvs_flash.h>
#include "driver/uart.h"

///////// WiFI ///////
#include "esp_wifi.h"
#include "esp_smartconfig.h"
#include "freertos/event_groups.h"
//#include "lwip/inet.h" // inet.pton() 

///////// TOUCH //////
#include "driver/touch_pad.h" 
#include "soc/rtc_periph.h"
#include "soc/sens_periph.h"

// For CV Clock out and Pitch 
#include "driver/ledc.h" 

///// MDNS //////
#include "mdns.h"//
#include "netdb.h" //

///// OSC ///////
#include <tinyosc.h> // still to test for speed !!

///// Socket /////
#include "lwip/sockets.h"

void initTDLF();

extern "C" {
  static const char *SOCKET_TAG;
  static const char *TOUCH_TAG = "Touch pad";
  static const char *SMART_TAG = "Smart config";
  static const char *NVS_TAG = "NVS";
  static const char *WIFI_TAG = "Wifi";
  static const char *LINK_TAG = "Link";
  static const char *TAP_TAG = "Tap";
  static const char *MIDI_TAG = "Midi";
  //static const char *CV_TAG = "CV";
  static const char *MDNS_TAG = "mDNS";
  static const char *TAG = "TAG";
  //static const char *SEQ_TAG = "Sequence";
}

///// socket /////
extern int sock; // We basically don't want to send via websocket...

extern struct sockaddr_in dest_addr; // Set up destination address
extern socklen_t socklen;
// memset(&dest_addr, 0, sizeof(dest_addr)); // memset needs to be in main


///// step /////
typedef struct {
    bool on;          
    uint8_t chan;
    uint8_t bar;
    uint8_t note;
    uint8_t length;
    bool mute;
} steps_t;

steps_t steps[64]; // Declare an array of type struct steps

// bool changeBPM;
extern bool changedMstr;
extern float duration[8];
extern float noteDuration; 
extern int myNoteDuration;
extern int bar[8];
extern int myBar;
extern bool muteRecords[8];
extern int stepsLength[4];

extern int beat;
extern int step;
extern int dubStep;
extern float oldstep;
extern int currStep;

extern esp_timer_handle_t periodic_timer;

extern bool isPlaying;
extern bool changePiton;
extern bool changeLink;
extern bool tempoINC;
extern bool tempoDEC;
extern double newBPM; // send to setTempo();
extern double oldBPM; 
extern double curr_beat_time;
extern double prev_beat_time;

extern bool connektMode;
extern char str_ip[16];

extern int nmbrSeq; // the number of sequences in nvs
extern char sequence[8][10];

extern int foisTapped;
extern bool toTapped;
extern int tapped;
extern int lastTapTime;
extern int tapInterval;
extern int tapArray[5];
extern int tapTotal;
extern int bpm;  // BPM in tenths of a BPM!!
extern int tappedBPM;
extern int minimumTapInterval;
extern int maximumTapInterval;

extern int test;
extern int channel; // 4 bits midi channel (0-15) -> // drums + más
extern int note; // 4 bits note info // 8 notes correspond to 8 colors // (0-7) -> (36,38,43,50,42,46,39,75),67,49 // más de 8 !
extern int mtmss[1264];
extern int CCChannel;
extern int sensorValue;
extern int sensorValue1;
extern int previousSensorValue;

extern bool startStopCB;
extern bool startStopState;

extern int nmbrClients;
extern int loadedClients;

///// sequences /////
extern bool loadedSeq[1264];
extern bool mstr[79];
extern bool oldmstr[79];

///////////// INTERACTIONS ///////////
/// save (Touch pad 2)
extern bool tapeArch;
extern bool saveBPM;
extern bool saveSeq;
extern bool seqSaved;
extern bool saveSeqConf;
extern bool loadSeq;
extern bool seqLoaded;
extern bool seqLoaded;
extern bool seqLoaded;
extern bool seqToLoad;
extern bool loadSeqConf;
extern bool saveDelay;
extern int delset;
extern int selectedSeq;

#endif
