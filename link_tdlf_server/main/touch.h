#ifndef TOUCH_H
#define TOUCH_H

#define USE_TOUCH_PADS // touch_pad_2 (GPIO_NUM_2), touch_pad_3 (GPIO_NUM_15), touch_pad_4 (GPIO_NUM_14), touch_pad_5 (GPIO_NUM_12), touch_pad_7 (GPIO_NUM_27), touch_pad_9 (GPIO_NUM_32)
//#define TOUCH_PAD_NO_CHANGE   (-1) // not necessary ?
#define TOUCH_THRESH_NO_USE   (0)
#define TOUCH_THRESH_PERCENT  (80) // 95
#define TOUCHPAD_FILTER_TOUCH_PERIOD (10) // 10

void initTouch();

#endif
