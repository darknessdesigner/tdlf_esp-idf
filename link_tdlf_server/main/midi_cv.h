#ifndef MIDI_CV_H
#define MIDI_CV_H

// CV Outputs //
#define CV_18  (GPIO_NUM_18) 
#define CV_5  (GPIO_NUM_5) 
#define GPIO_OUTPUT_PIN_SEL  ( (1ULL<<CV_18) | (1ULL<<CV_5) )
int CV_TIMING_CLOCK = 0; 

// Serial midi
#define ECHO_TEST_RTS (UART_PIN_NO_CHANGE)
#define ECHO_TEST_CTS (UART_PIN_NO_CHANGE)
#define ECHO_TEST_TXD  (GPIO_NUM_26) // was TTGO touch  pin 4 (GPIO_NUM_17) // 13 // change for GPIO_NUM_17 // GPIO_NUM_26
#define ECHO_TEST_RXD  (GPIO_NUM_19) 
#define BUF_SIZE (1024)
#define MIDI_TIMING_CLOCK 0xF8
#define MIDI_NOTE_OFF 0x80 // 10000000 // 128
#define MIDI_START 0xFA // 11111010 // 250
#define MIDI_STOP 0xFC // 11111100 // 252

char MIDI_NOTE_ON_CH[] = {0x90,0x91,0x92,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9A,0x9B,0x9C,0x9D,0x9E,0x9F}; // note on, channel 10, note on, channel 0 // ajouter d'autres séries

//////// CC messages config //////////
// Load MIDI CC config from NVS if it exists and turn this off is a cfg exists //
// We might not need to do much config of CC messages as the sensor sends it's own Channel and CC values
bool need2configCC = true; // Keep an eye out for sensor messages, if so, start the config of midi CC messages
bool configCC = false; // maybe only one of these two is needed !
bool configCCChannel = false;
int CCChannel1 = 1;
int prevCCmessage = 0;
int compteur = 0;
int CCmessages[9] = {0};
char MIDI_CONTROL_CHANGE_CH[] = {0xB0,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB9,0xBA,0xBB,0xBC,0xBD,0xBE,0xBF}; // send control change on channel 1-16, 
bool configCCmessage = false;
int CCmessage = 0;
int CCmessage1 = 0;
char MIDI_CONTROL_NUMBER[] = {0x36,0x37}; // stutter time (volca beats), stutter depth (volca beats) // find another way to cfg this

char MIDI_NOTES[16]; // keep notes in memory along with interval at which to trigger the note off message
int MIDI_NOTES_DELAYED_OFF[16] = {0};
int CV_TRIGGER_OFF[16] = {0};

char zeDrums[] = {0x24,0x26,0x2B,0x32,0x2A,0x2E,0x27,0x4B,0x43,0x31}; // midi drum notes in hexadecimal format
char zeDark[] = {0x3D,0x3F,0x40,0x41,0x42,0x44,0x46,0x47}; // A#(70)(0x46), B(71)(0x47), C#(61)(0x3D), D#(63)(0x3F), E(64)(0x40), F(65)(0x41), F#(66)(0x42), G#(68)(0x44)

// octave : C |C# D |D# E F |F# G|G# A|A# B 

/*
0x24 // 36 // C2 //  Kick
0x26 // 38 // D2 //  Snare
0x2B // 43 // G2 //  Low tom
0x32 // 50 // D3 //  Hi Tom 
0x2A // 42 // F#2 // Closed Hat 
0x2E // 46 // A#2 // Open Hat
0x27 // 39 // D#2 // Clap 
0x4B // 75 // D#5 // Claves 
0x43 // 67 // G4 //  Agogo 
0x31 // 49 // C#3 // Crash
*/

// penser à un système qui réarrange les notes selon les gammes, on a huit notes...
// est-ce que l'utilisateur peut transposer d'octave...sans doute

#define MIDI_NOTE_VEL 0x64 // 1100100 // 100 // note on,  // data
#define MIDI_NOTE_VEL_OFF 0x00 // 0 // note off
#define MIDI_SONG_POSITION_POINTER 0xF2

static void periodic_timer_callback(void* arg)
{
    char zedata[] = { MIDI_TIMING_CLOCK };
    // ESP_LOGI(LINK_TAG, "MIDI_TIMING_CLOCK");
    uart_write_bytes(UART_NUM_1, zedata, 1);

    if(CV_TIMING_CLOCK % 6 == 0 ){ // faire modulo 6 pour arriver à 4 ticks par beat // 24 times per second
      gpio_set_level(CV_5, 1); // CV Clock Out
      // ESP_LOGI(CV_TAG, "CLOCK 1");
      CV_TIMING_CLOCK = 0;
    } else {
      gpio_set_level(CV_5, 0); 
      // ESP_LOGI(CV_TAG, "CLOCK 0");
    }  
    CV_TIMING_CLOCK = CV_TIMING_CLOCK+1;
    // ESP_LOGI(CV_TAG, "CV_TIMING_CLOCK, %i", CV_TIMING_CLOCK);
}

void initMidiSerial(){
  uart_config_t uart_config = {
    .baud_rate = 31250, // midi speed
    .data_bits = UART_DATA_8_BITS,
    .parity    = UART_PARITY_DISABLE,
    .stop_bits = UART_STOP_BITS_1,
    .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
    .rx_flow_ctrl_thresh = 122,
  };
  uart_param_config(UART_NUM_1, &uart_config);
  uart_set_pin(UART_NUM_1, ECHO_TEST_TXD, ECHO_TEST_RXD, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
  uart_driver_install(UART_NUM_1, BUF_SIZE * 2, 0, 0, NULL, 0);
 
}
#endif