#include "tdlf.h"

#ifndef SOCKET_SERVER_H
#define SOCKET_SERVER_H

namespace {
  #define OSC_BROADCAST_ADDR "192.168.0.102" // Hard-coded to send data to MaxD //
  #define OSC_BROADCAST_PORT 3333
  #define MDNS_PORT 5353
}

char addr_str[128]; // Copié depuis le client
int addr_family;   

int ip_protocol;
char clientIPAddresses[8][22]; // 8 potential clients, IPv6 format + 1 for string termination by strncat
  

int clientIPCheck ( char myArray[] ) { // ESP_LOGI(SOCKET_TAG, "This is myArray : %s", myArray);
    
  for ( int i = 0; i < 8; i++ ) { // max of 8 IP addresses connected
    if( strcmp( myArray, clientIPAddresses[i] ) == 0 ) {
      return i; // IP Address already exists at position 'i' in the array
      break; 
      }

  }
  return 42; // not in the array, add it
}

extern "C" {
  static void udp_server_task(void *pvParameters)
  {

    char mstr[79];
    char rx_buffer[128];

    // struct sockaddr_in dest_addr; // Set up destination address
    // socklen_t socklen = sizeof(dest_addr);
    // memset(&dest_addr, 0, sizeof(dest_addr)); // memset needs to be in main
    // dest_addr.sin_family = AF_INET;
    // dest_addr.sin_addr.s_addr = inet_addr(OSC_BROADCAST_ADDR); //  "192.168.0.255"
    // // dest_addr.sin_addr.s_addr = htonl(INADDR_ANY); // Bind to any available network interface
    // dest_addr.sin_port = htons(OSC_BROADCAST_PORT);
    // inet_pton(AF_INET, OSC_BROADCAST_ADDR, &dest_addr.sin_addr);

    //bzero(&dest_addr.sin_addr.un, sizeof(dest_addr.sin_addr.un));

  
    while(1){
      if(sock == 666){
      
        sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP); // Was IPPROTO_IP // Create UDP socket
        // sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP); // Create UDP socket
        // sock = socket(addr_family, SOCK_DGRAM, ip_protocol);

        if (sock < 0) {
          ESP_LOGE(SOCKET_TAG, "Unable to create socket: errno %d", errno);
          return;
        } else{
          ESP_LOGE(SOCKET_TAG, "Socket created : %d", sock);  
        }

        const char *message = "Hello, UDP!";
        int err = sendto(sock, message, strlen(message), 0, (struct sockaddr *)&dest_addr, sizeof(dest_addr));
        if (err < 0) {
          ESP_LOGE(TAG, "Error sending UDP message: %d", errno);
        }

       int err1 = bind(sock, (struct sockaddr *)&dest_addr, sizeof(dest_addr));
        if (err1 < 0) {
          close(sock);
          ESP_LOGE(SOCKET_TAG, "Socket unable to bind: errno %d", errno);
          return;
        } else {
          ESP_LOGI(SOCKET_TAG, "Socket %d bound", sock);
        }

      } // fin if sock == 666

    while (1) {

      ESP_LOGI(SOCKET_TAG, "Waiting for data\n");
      ESP_LOGI(SOCKET_TAG, "sock : %d", sock);

      struct sockaddr_in source_addr; 
      socklen_t socklen = sizeof(source_addr);

      ESP_LOGI(SOCKET_TAG, "rx_buffer : %s", rx_buffer);
      ESP_LOGI(SOCKET_TAG, "rx_buffer : %d", sizeof(rx_buffer));

      int len = recvfrom(sock, rx_buffer, sizeof(rx_buffer), 0, (struct sockaddr *)&source_addr, &socklen);
      
      ESP_LOGI(SOCKET_TAG, "après recvfrom?\n");
        
      ESP_LOGI(SOCKET_TAG, "len : %d", len);  

      if (len > 0){
        ESP_LOGE(SOCKET_TAG, "len %i", len); 
        // for(int i = 0; i<sizeof(mstr); i++){
        //   test = mstr[i];
        //   ESP_LOGE(SOCKET_TAG, "data %i", test); 
        // }  
          test = mstr[0];
          //ESP_LOGE(SOCKET_TAG, "testing mstr[0] %i", test); 
          //if (test==0||test==1){

        if (test<42){
          ESP_LOGE(SOCKET_TAG, "got an an old array"); 
          ///// midi channel ///// 1-16 midi channels
          channel = mstr[13];
          ESP_LOGI(SOCKET_TAG, "channel : %i", channel); 

          ///// note ///// use this for solenoids!
          //note = mstr[2]; // Which instrument is playing ()
          note = mstr[14];
          ESP_LOGI(SOCKET_TAG, "note : %i", note); 

          //steps[mstr[16]].on = mstr[15]; // m[2] is on/off info and m[3] is the step number

          ///// noteDuration //////
          // duration = mstr[3];
          // ESP_LOGI(SOCKET_TAG, "noteDuration : %f", noteDuration); 

          ///// bar //////
          // bar = mstr[4];
          // ESP_LOGI(SOCKET_TAG, "noteDuration : %f", noteDuration); 

          ///// mute ///// NIY
          // mute = mstr[5];
          // ESP_LOGI(SOCKET_TAG, "mute : %f", mute); 
          // read in the bit value for mute and store 
          // muteRecords[note] = mstr[14]; // redo this the position has changed in the mstr array
          // ESP_LOGI(SOCKET_TAG, "mute ? : %i", mstr[10]);  

          ///// erase part ///// NIY
          // erasePart = mstr[6];
          // ESP_LOGI(SOCKET_TAG, "erasePart : %f", erasePart); 

          ///// erase all ///// NIY
          // eraseAll = mstr[7];
          // ESP_LOGI(SOCKET_TAG, "eraseAll : %f", eraseAll);

              
          for (int i = 0; i <=79; i++){
            if (note == 36) { // Volca notes 36,38,43,50,42,46,39,75
              note = 0;
              } else if (note == 38) {
              note = 1;
              } else if (note == 43) {
              note = 2;
              } else if (note == 50) {
              note = 3;
              } else if (note == 42) {
              note = 4;
              } else if (note == 46) {
              note = 5;
              } else if (note == 39) {
              note = 6;
              } else if (note == 75) {
              note = 7;
              }

              mtmss[i+note*79] = mstr[i];
              //ESP_LOGI(SOCKET_TAG, "hello mtmss : %i", mtmss[i]); 
              //ESP_LOGI(SOCKET_TAG, "hello mstr !!!!! %i : %i", i, mstr[i]);
            } 

            // depending on note number, enter this value in mtmss

            } else {
              //ESP_LOGI(SOCKET_TAG, "testing testing"); 
              /*  for (int i = 0; i <=79; i++){
                mtmss[i+note*79] = mstr[i];
                //ESP_LOGI(SOCKET_TAG, "hello mtmss !!!!! : %i", mtmss[i]); 
                ESP_LOGI(SOCKET_TAG, "hello mstr !!!!! %i : %i", i, mstr[i]);
              } */
              /////////////////// FROM TINYOSC ///////////////////////
              //tosc_printOscBuffer(rx_buffer, len);
              tosc_printOscBuffer(mstr, len);
              tosc_message osc; // declare the TinyOSC structure
              // char buffer[1024]; // char buffer[64]; declare a buffer into which to read the socket contents
              //int oscLen = 0; // the number of bytes read from the socket

              // tosc_parseMessage(&osc, rx_buffer, len);
              tosc_parseMessage(&osc, mstr, len);

              //tosc_printMessage(&osc);
              tosc_getAddress(&osc),  // the OSC address string, e.g. "/button1"
              tosc_getFormat(&osc);  // the OSC format string, e.g. "f"

              ////&osc->len,              // the number of bytes in the OSC message
              //tosc_getAddress(&osc),  // the OSC address string, e.g. "/button1"
              //tosc_getFormat(&osc));  // the OSC format string, e.g. "f"
              
              // ESP_LOGI(SOCKET_TAG, " Unknown format: %c", osc.format[0]);
              unsigned char *m = tosc_getNextMidi(&osc);
              //printf(" 0x%02X%02X%02X%02X", m[0], m[1], m[2], m[3]);
              printf("%02i, %02i, %02i, %02i\n", m[0], m[1], m[2], m[3]);

              // the following is for sending CC messages
            
              CCChannel = m[0];
              sensorValue = m[1];
              CCmessage = m[2];

              char zedata01[] = { MIDI_CONTROL_CHANGE_CH[CCChannel-1] }; // send CC message on midi channel 1 '0xB0'
              uart_write_bytes(UART_NUM_1, zedata01, 1); // this function will return after copying all the data to tx ring buffer, UART ISR will then move data from the ring buffer to TX FIFO gradually.
        
              char zedata02[] = { (char)CCmessage};  //  '0x36' (54) trying for Volca Beats Stutter time
              uart_write_bytes(UART_NUM_1, zedata02, 1); 
        
              char zedata03[] = { (char)sensorValue }; // need to convert sensorValue to hexadecimal! 
              uart_write_bytes(UART_NUM_1, zedata03, 1); 

              // if (m[2] != 42){
              //   CCChannel1 = m[0];
              //   sensorValue1 = m[1];
              //   CCmessage1 = m[2];
              //   } else {
              //     CCChannel = m[0];
              //     printf("CCmessage : %02i\n", m[2]);
              //     sensorValue = m[1];
              //     CCmessage = m[2];  
              //     }

              //     steps[m[3]].on = m[2]; // m[2] is on/off info and m[3] is the step number

            //switch (tosc_getFormat(&osc)) {
              // switch(osc.format[0]) {
      
              //     //case 'i': {
              //     case 'm': {
              //         unsigned char *m = tosc_getNextMidi(&osc);
              //         //printf(" 0x%02X%02X%02X%02X", m[0], m[1], m[2], m[3]);
              //         //printf("\n");
              //         // the following is for sending CC messages
              //         CCChannel = m[0];
              //         sensorValue = m[1];
              //         CCmessage = m[2];
              //         steps[m[3]].on = m[2]; // m[2] is on/off info and m[3] is the step number

              //         // If the sequencer is going to only be 16 steps...we might not need all these 64...
              //         /* for (int i = 0 ; i < 64 ; i++ ){
              //           ESP_LOGI(SEQ_TAG, "step : %i, note value :%i", i, steps[i].on); 
              //         } */

              //         //printf("%02X",m[0]);
              //         //printf("\n");
        
              //         break;
              //     }
      
              //     default:
              //         printf(" Unknown format: '%c'", osc.format[0]);
              //         break;
              //     } 
    
                  printf("\n");
              /////////////////// END TINYOSC ///////////////////////
            }

              ///// Step ///// 0-63 steps
              //ESP_LOGI(SOCKET_TAG, "note : %i", note); // Change this
              // Where is the note going in the sequence
            
              // Error occurred during receiving ?
              if (len < 0) {
                  ESP_LOGE(SOCKET_TAG, "recvfrom failed: errno %d", errno);
                  break;
                }
              
              else {   // Data received
                  inet6_ntoa_r(source_addr.sin_addr, addr_str, sizeof(addr_str) - 1); // Get the sender's ip address as string
                  //ESP_LOGI(SOCKET_TAG, "This is the address : %s", addr_str);
                }
                  
              ESP_LOGI(SOCKET_TAG, "Received %d bytes from %s:", len, addr_str);
              //inet6_ntoa_r(source_addr.sin_addr, addr_str, sizeof(addr_str) - 1); // Get the sender's ip address as string
                  
              if ( startStopState == false ) { // only check for clients if we are in stopped mode, it hangs the playback otherwise...
                
                int checkIPExist = clientIPCheck(addr_str); // Does it exist in the array?

              // ESP_LOGI(SOCKET_TAG, "result of clientIPCheck : %i", checkIPExist);

                if ( checkIPExist == 42 ) { // if it doesn't exist, add it
                
                  strncat(clientIPAddresses[nmbrClients], inet6_ntoa_r(source_addr.sin_addr, addr_str, sizeof(addr_str) - 1),21); // add that address to the array 
                  ESP_LOGI(SOCKET_TAG, "Added client address : %s", clientIPAddresses[nmbrClients]);

                  nmbrClients++; // Count the newly registered client
                  ESP_LOGI(SOCKET_TAG, "How many clients ? : %i", nmbrClients);

                  }

                else { // IP already exists 

                  // ESP_LOGI(SOCKET_TAG, "Address already exists : %s", addr_str);    // ip address already exists in the array so do nothing // at what position
                  
                  if ( seqToLoad ) { // 

                    ESP_LOGI(SOCKET_TAG, "Sending love instead of an IP to : %s", addr_str); 

                    bool tmpArray[79];
                    
                    for ( int i = 0; i < 80; i++ ) { // checkIPExist is the offset

                      tmpArray[i] = loadedSeq[i+(79*checkIPExist)]; // populate tmpmstr array
                      ESP_LOGI(SOCKET_TAG, "tmpArray[%i] = %i ", i, tmpArray[i]); 

                    }

                    // int err = sendto(sock, tmpArray, sizeof(tmpArray), 0, (struct sockaddr *)&source_addr, sizeof(source_addr));
                    loadedClients++; // keep track of how many clients have been loaded
                    ESP_LOGI(SOCKET_TAG, "loadedClients : %i\n", loadedClients); 
                    
                    if( loadedClients == nmbrClients ) { // All the clients are loaded

                      seqToLoad = false;
                      loadedClients = 0;

                      }
                      
                  }

                } // End of "else if IP exists"

                // ESP_LOGI(SOCKET_TAG, "nmbrClients : %i\n", nmbrClients); 
              
              } 

              //int err = sendto(sock, str_ip, sizeof(str_ip), 0, (struct sockaddr *)&source_addr, sizeof(source_addr));
              //ESP_LOGI(SOCKET_TAG, "Sent my IP %s", str_ip); 

          } // check sur len

          //portYIELD();
          //vTaskDelay(2 / portTICK_PERIOD_MS);
          ESP_LOGI(SOCKET_TAG, "dans le deuxième while...");
        } // End of 2nd while
        if (sock != -1) {
          ESP_LOGI(SOCKET_TAG, "Shutting down socket and restarting...");
          shutdown(sock, 0);
          close(sock);
          }
      } // End of 1st while
      vTaskDelete(NULL);
      ESP_LOGI(SOCKET_TAG, "après vtask");
    } // Fin de udp server task
  } // Fin de extern "C"

  #endif
