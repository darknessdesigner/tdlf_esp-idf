// AbletonLink utility functions
#include "tdlf.h"
#include "ableton_.h"

void IRAM_ATTR timer_group0_isr(void* userParam)
{
  static BaseType_t xHigherPriorityTaskWoken = pdFALSE;

  TIMERG0.int_clr_timers.t0 = 1;
  TIMERG0.hw_timer[0].config.alarm_en = 1;

  xSemaphoreGiveFromISR(userParam, &xHigherPriorityTaskWoken);
  if (xHigherPriorityTaskWoken)
  {
    portYIELD_FROM_ISR();
  }
}

void timerGroup0Init(int timerPeriodUS, void* userParam)
{
  timer_config_t config = {.alarm_en = TIMER_ALARM_EN,
    .counter_en = TIMER_PAUSE,
    .intr_type = TIMER_INTR_LEVEL,
    .counter_dir = TIMER_COUNT_UP,
    .auto_reload = TIMER_AUTORELOAD_EN,
    .divider = 80};

  timer_init(TIMER_GROUP_0, TIMER_0, &config);
  timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0);
  timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, timerPeriodUS);
  timer_enable_intr(TIMER_GROUP_0, TIMER_0);
  timer_isr_register(TIMER_GROUP_0, TIMER_0, &timer_group0_isr, userParam, 0, nullptr);

  timer_start(TIMER_GROUP_0, TIMER_0);
}

// Ableton callbacks
void tempoChanged(double tempo) {
    ESP_LOGI(LINK_TAG, "tempochanged");
    double midiClockMicroSecond = ((60000 / tempo) / 24) * 1000;
    esp_timer_handle_t periodic_timer_handle = (esp_timer_handle_t) periodic_timer;
    ESP_ERROR_CHECK(esp_timer_stop(periodic_timer_handle));
    ESP_ERROR_CHECK(esp_timer_start_periodic(periodic_timer_handle, midiClockMicroSecond));
}

static void startStopChanged(bool state) {
  // received as soon as sent, we can get the state of 'isPlaying' and use that
  // need to wait for phase to be 0 (and deal with latency...)
  startStopCB = state;
  changeLink = true;
  //ESP_LOGI(LINK_TAG, "startstopCB is : %i", state);
  //ESP_LOGI(LINK_TAG, "changeLink is : %i", state);
}

void delayer(int del) {
  delset = esp_timer_get_time()+del;
  ESP_LOGI(TAP_TAG, "delaying until : %i", delset); 
}

void initTickTask(){
  SemaphoreHandle_t tickSemphr = xSemaphoreCreateBinary();
  timerGroup0Init(1000, tickSemphr);
  xTaskCreate(tickTask, "tick", 8192, tickSemphr, 1, nullptr); // The abletonLink task
}


// ableton task
void tickTask(void* userParam)
{
  // connect link
  ableton::Link link(120.0f);
  link.enable(true);
  link.enableStartStopSync(true); // if not no callback for start/stop

  // callbacks
  link.setTempoCallback(tempoChanged);
  link.setStartStopCallback(startStopChanged);

  // phase
  while (true)
  { 
    xSemaphoreTake((QueueHandle_t)userParam, portMAX_DELAY);
    const auto state = link.captureAudioSessionState(); 
    isPlaying = state.isPlaying();
    //ESP_LOGI(LINK_TAG, "isPlaying : , %i", isPlaying);  
    //const auto phase = state.phaseAtTime(link.clock().micros(), 1); 
    //ESP_LOGI(LINK_TAG, "tempoINC : %i", tempoINC);
    //ESP_LOGI(LINK_TAG, "tempoDEC : %i", tempoDEC);
    //ESP_LOGI(LINK_TAG, "saveSeqConf : %i", saveSeqConf);
    //ESP_LOGI(LINK_TAG, "toTapped : %i", toTapped );

    if ( saveSeqConf == true ) {
      writeSeqNVS();
    }

    if ( loadSeqConf == true ) {
      loadSeqNVS();
    }

    /*if ( changeBPM == true ) {
      auto mySession = link.captureAppSessionState();
      const auto timez = link.clock().micros();
      mySession.setTempo(newBPM,timez); // setTempo()'s second arg format is : const std::chrono::microseconds atTime
      link.commitAppSessionState(mySession); 
      const auto tempo = state.tempo(); // quelle est la valeur de tempo?
      myNoteDuration = ((60/(tempo*4))*1000)*noteDuration; // calculate noteDuration as a function of BPM // 60 BPM * 4 steps per beat = 240 steps per minute // 60 seconds / 240 steps = 0,25 secs or 250 milliseconds per step // ((60 seconds / (BPM * 4 steps per beat))*1000 ms)*noteDuration
      ESP_LOGI(LINK_TAG, "changeBPM changed %i", int(newBPM));
      
      ///// send new BPM to clients /////
      char monBufferBPM[16]; 

      int maLen = tosc_writeMessage(
      monBufferBPM, 
      sizeof(monBufferBPM),
      "/bpm", // the address // /a second level like /tdlf/step returns an error '122'
      "i",   // the format; 'f':32-bit float, 's':ascii string, 'i':32-bit integer
      int(newBPM)
      ); 
      
      // ESP_LOGE(SOCKET_TAG, "maLen : %d; sock :%d", maLen, sock);
      int err = sendto(sock, monBufferBPM, maLen, 0, (struct sockaddr *)&dest_addr, sizeof(dest_addr));

      if (err < 0) {
        ESP_LOGE(TAG, "Error sending UDP mess: %d", errno);
        printf("err = %i\n", err);
        printf("%s", esp_err_to_name(err));
        printf("\n");
        }
      ///////////////////////////////////

      oldBPM = newBPM; // store this to avoid spurious calls
    }*/

    if ( tempoINC == true ) {
      const auto tempo = state.tempo(); // quelle est la valeur de tempo?
      newBPM = tempo + 1;
      ESP_LOGI(LINK_TAG, "BPM changed %i", int(newBPM));
      oldBPM = newBPM; // store this to avoid spurious calls
      auto mySession = link.captureAppSessionState();
      const auto timez = link.clock().micros();
      mySession.setTempo(newBPM,timez); // setTempo()'s second arg format is : const std::chrono::microseconds atTime
      link.commitAppSessionState(mySession); // le problème est que l'instruction de changer le tempo nous revient
      ///// send new BPM to clients /////
      char monBufferBPM[16]; 

      int maLen = tosc_writeMessage(
      monBufferBPM, 
      sizeof(monBufferBPM),
      "/bpm", // the address // /a second level like /tdlf/step returns an error '122'
      "i",   // the format; 'f':32-bit float, 's':ascii string, 'i':32-bit integer
      int(newBPM)
      ); 
      
      // ESP_LOGE(SOCKET_TAG, "maLen : %d; sock :%d", maLen, sock);
      int err = sendto(sock, monBufferBPM, maLen, 0, (struct sockaddr *)&dest_addr, sizeof(dest_addr));

      if (err < 0) {
        ESP_LOGE(TAG, "Error sending UDP mess: %d", errno);
        printf("err = %i\n", err);
        printf("%s", esp_err_to_name(err));
        printf("\n");
        }
      ///////////////////////////////////

      tempoINC = false;
      saveBPM = false; // as changing the BPM here implies not wanting to tap the tempo in
      myNoteDuration = ((60/(tempo*4))*1000)*noteDuration; // calculate noteDuration as a function of BPM // 60 BPM * 4 steps per beat = 240 steps per minute // 60 seconds / 240 steps = 0,25 secs or 250 milliseconds per step // ((60 seconds / (BPM * 4 steps per beat))*1000 ms)*noteDuration
      //ESP_LOGI(LINK_TAG, "tempoINC : %i", tempoINC);
    }

    if ( tempoDEC == true ) {
      const auto tempo = state.tempo(); // quelle est la valeur de tempo?
      newBPM = tempo - 1;
      ESP_LOGI(LINK_TAG, "BPM changed %i", int(newBPM));
      oldBPM = newBPM; // store this to avoid spurious calls
      auto mySession = link.captureAppSessionState();
      const auto timez = link.clock().micros();
      mySession.setTempo(newBPM,timez); // setTempo()'s second arg format is : const std::chrono::microseconds atTime
      link.commitAppSessionState(mySession);
      ///// send new BPM to clients /////
      char monBufferBPM[16]; 

      int maLen = tosc_writeMessage(
      monBufferBPM, 
      sizeof(monBufferBPM),
      "/bpm", // the address // /a second level like /tdlf/step returns an error '122'
      "i",   // the format; 'f':32-bit float, 's':ascii string, 'i':32-bit integer
      int(newBPM)
      ); 
      
      // ESP_LOGE(SOCKET_TAG, "maLen : %d; sock :%d", maLen, sock);
      int err = sendto(sock, monBufferBPM, maLen, 0, (struct sockaddr *)&dest_addr, sizeof(dest_addr));

      if (err < 0) {
        ESP_LOGE(TAG, "Error sending UDP mess: %d", errno);
        printf("err = %i\n", err);
        printf("%s", esp_err_to_name(err));
        printf("\n");
        }
      ///////////////////////////////////

      tempoDEC = false;
      saveBPM = false;
      myNoteDuration = ((60/(tempo*4))*1000)*noteDuration; // calculate noteDuration as a function of BPM // 60 BPM * 4 steps per beat = 240 steps per minute // 60 seconds / 240 steps = 0,25 secs or 250 milliseconds per step // ((60 seconds / (BPM * 4 steps per beat))*1000 ms)*noteDuration
    }

    ////////// test start stop send to other clients /////////
    //ESP_LOGI(LINK_TAG, "PRE startStopState is :  %i", startStopState);  
    //ESP_LOGI(LINK_TAG, "PRE startStopCB is :  %i", startStopCB); 

    if ( changePiton && startStopState != startStopCB ){ // if local state is different to the SessionState then send to the session state 
        auto mySession = link.captureAppSessionState();
        const auto timez = link.clock().micros();
        mySession.setIsPlaying(startStopState, timez);
        link.commitAppSessionState(mySession);
      }
      
    if ( changeLink && startStopState != startStopCB ){ // if CB state is different to the local startStopState then resync the latter to the former
       startStopState = startStopCB; // resync 
      }

    if( toTapped == true ){
      tapped = esp_timer_get_time()/1000;
      tapInterval = tapped - lastTapTime;
      // ESP_LOGI(TAP_TAG, "Time at tap: %ld ", tapped);
      ESP_LOGI(TAP_TAG, "Time at tap: %i ", tapped);
      ESP_LOGI(TAP_TAG, "Interval between taps n: %i ", tapInterval );
      if(tapInterval > maximumTapInterval){ // reset tapCounter to zero
        foisTapped = 0;
      }

      if(minimumTapInterval < tapInterval && tapInterval < maximumTapInterval){ // only add a tap if we are within the tempo bounds
        foisTapped = foisTapped + 1;
        ESP_LOGI(TAP_TAG, "foisTapped %i", foisTapped);
        // add a tapInterval to the sliding values array
        tapArray[foisTapped%5] = tapInterval;
      }
      
      if(foisTapped > 5){  // calculate BPM from tapInterval
        for(int i = 0; i<6; i++){
          tapTotal = tapArray[i] + tapTotal;
          ESP_LOGI(TAP_TAG, "tapped interval at %i, %i", i, tapArray[i]);

        }
        tapTotal = tapTotal / 6;
        tappedBPM = 60L * 1000 / tapTotal;
        ESP_LOGI(TAP_TAG, "tapped BPM %i", tappedBPM);
        tapTotal = 0;
        saveBPM = true;
        saveDelay = true; // appeler une fonction pour remettre les variables à false après un moment
        delayer(3000000);
      }

      lastTapTime = tapped;
      toTapped = false;
    }

    if (esp_timer_get_time() > delset ){
     saveBPM = false;
     saveSeq = false; 
     seqSaved = false;
     loadSeq = false;
     seqLoaded = false;
   }

    if ( saveBPM == true && tapeArch == true ) {
      auto mySession = link.captureAppSessionState();
      const auto timez = link.clock().micros();
      mySession.setTempo(tappedBPM,timez); // setTempo()'s second arg format is : const std::chrono::microseconds atTime
      link.commitAppSessionState(mySession);
      saveBPM = false;
      tapeArch = false;
    }

    //ESP_LOGI(LINK_TAG, "POST startStopState is :  %i", startStopState);  
    //ESP_LOGI(LINK_TAG, "POST startStopCB is :  %i", startStopCB); 
   
    curr_beat_time = state.beatAtTime(link.clock().micros(), 4);
    const double curr_phase = fmod(curr_beat_time, 4);  // const double

    if ( curr_beat_time > prev_beat_time ) {

      // try sending cc messages here 
      if (sensorValue > -42 && configCC == false){

        // char zedata1[] = { MIDI_CONTROL_CHANGE_CH[CCChannel-1] }; // send CC message on midi channel 1 '0xB0'
        // uart_write_bytes(UART_NUM_1, zedata1, 1); // this function will return after copying all the data to tx ring buffer, UART ISR will then move data from the ring buffer to TX FIFO gradually.
      
        // char zedata2[] = { (char)CCmessage};  //  '0x36' (54) trying for Volca Beats Stutter time
        // uart_write_bytes(UART_NUM_1, zedata2, 1); 
      
        // char zedata3[] = { (char)sensorValue }; // need to convert sensorValue to hexadecimal! 
        // uart_write_bytes(UART_NUM_1, zedata3, 1); 

        // // Hackish
        // char zedata4[] = { MIDI_CONTROL_CHANGE_CH[CCChannel1-1] }; // send CC message on midi channel 1 '0xB0'
        // uart_write_bytes(UART_NUM_1, zedata4, 1); // this function will return after copying all the data to tx ring buffer, UART ISR will then move data from the ring buffer to TX FIFO gradually.
      
        // char zedata5[] = { (char)CCmessage1};  //  '0x36' (54) trying for Volca Beats Stutter time
        // uart_write_bytes(UART_NUM_1, zedata5, 1); 
      
        // char zedata6[] = { (char)sensorValue1 }; // need to convert sensorValue to hexadecimal! 
        // uart_write_bytes(UART_NUM_1, zedata6, 1); 

        if (sensorValue != previousSensorValue){
          // ESP_LOGI(CV_TAG, "CV_PITCH, %i", int(sensorValue));
          ledc_set_duty(ledc_channel.speed_mode, ledc_channel.channel, int(sensorValue)); // CV_PITCH
          ledc_update_duty(ledc_channel.speed_mode, ledc_channel.channel);
          previousSensorValue = sensorValue;
        }
      }
      
      const double prev_phase = fmod(prev_beat_time, 4);
      const double prev_step = floor(prev_phase * 4);
      const double curr_step = floor(curr_phase * 4);
      
      //ESP_LOGI(LINK_TAG, "current step : %f", curr_step); 
      //ESP_LOGI(LINK_TAG, "prev step : %f", prev_step); 
     
      if (abs(prev_phase - curr_phase) > 4 / 2 || prev_step != curr_step) { // quantum divisé par 2

        if( ( curr_step == 0 && changePiton ) || ( curr_step == 0 && changeLink ) ) { // start / stop implementation
            
              if(startStopCB) { // saw a simpler way of doing this!
                char zedata[] = { MIDI_START };
                uart_write_bytes(UART_NUM_1, zedata, 1);
                //uart_write_bytes(UART_NUM_1, 0, 1); // ? produces : E (24513) uart: uart_write_bytes(1112): buffer null
                changeLink = false;
                changePiton = false;
                step = 0; // reset step count
              } 
              else { // on jouait et on arrête // changer pout s'arrêter immédiatement après un stop 
                char zedata[] = { MIDI_STOP };
                uart_write_bytes(UART_NUM_1, zedata, 1);
                //uart_write_bytes(UART_NUM_1, 0, 1);
                changeLink = false;
                changePiton = false;
                }
            }
  

        if (step == 64){ // max steps = 64
          step = 0;
          }

        if(startStopCB){
          SSD1306_SetInverted( &I2CDisplay, 1);
          }
          
        if(!startStopCB){
          SSD1306_SetInverted( &I2CDisplay, 0);  
          }

        const int halo_welt = int(curr_phase);

        const int tmpOH = (int) round( state.tempo() );
        // ESP_LOGI(LINK_TAG, "tempo %i", tmpOH); 

        char phases[5] = "TDLF";

        char tmpOHbuf[20];
        char top[20];

        if ( seqSaved == true ) {
          snprintf(tmpOHbuf, 20 , "Sequence"); 
          snprintf(current_phase_step, 20, "Saved   ");
        }

        else if ( saveSeq == true ) {
          snprintf(tmpOHbuf, 20 , "Sequence"); 
          snprintf(current_phase_step, 20, "Save?   ");
        }

        else if ( seqLoaded == true ) {
          snprintf(tmpOHbuf, 20 , "Sequence"); 
          snprintf(current_phase_step, 20, "%i loaded", selectedSeq);
        }

        else if ( loadSeq == true ) {
          snprintf(tmpOHbuf, 20 , "Sequence"); 
          snprintf(current_phase_step, 20, "Load:+%i-?", selectedSeq);
        }
        
        else if (saveBPM == false){
          snprintf(tmpOHbuf, 20 , "%i BPM", tmpOH );     /////// display BPM + Phase + Step /////////

          if (step < 10){
            snprintf(current_phase_step, 20, " 0%i STP", step);
          }
          else {
            snprintf(current_phase_step, 20, " %i STP", step);
          }
        }
        
        else if (saveBPM == true){ // we have a tapped BPM ready to switch?
          snprintf(tmpOHbuf, 20 , "%i BPM?", tappedBPM ); 
          
          if (step < 10){
            snprintf(current_phase_step, 20, " 0%i STP", step);
          }
          else {
            snprintf(current_phase_step, 20, " %i STP", step);
          }
        }

      switch (halo_welt)
      {
        case 0:
        strcpy(phases, "X000");
        break;
        case 1:
        strcpy(phases, "XX00");
        break;
        case 2:
        strcpy(phases, "XXX0"); 
        break;
        case 3:
        strcpy(phases, "XXXX");
        break;
        default:
        ESP_LOGI(LINK_TAG, "phases not assigned correctly"); 
      }

        snprintf(top, 20, "clients:%i  %s", nmbrClients, phases);
        SSD1306_Clear( &I2CDisplay, SSD_COLOR_BLACK );
        SSD1306_SetFont( &I2CDisplay, &Font_droid_sans_mono_7x13);
        SSD1306_FontDrawAnchoredString( &I2CDisplay, TextAnchor_North, top, SSD_COLOR_WHITE ); 
        SSD1306_SetFont( &I2CDisplay, &Font_droid_sans_mono_13x24); // &Font_droid_sans_mono_13x24 // &Font_droid_sans_fallback_15x17
        SSD1306_FontDrawAnchoredString( &I2CDisplay, TextAnchor_Center, tmpOHbuf, SSD_COLOR_WHITE );
        SSD1306_FontDrawAnchoredString( &I2CDisplay, TextAnchor_South, current_phase_step, SSD_COLOR_WHITE );
        SSD1306_Update( &I2CDisplay );  

        if (startStopCB){ // isPlaying and did we send that note out?
          //// Send step via OSC here ////
          char monBuffer[16]; // monBuffer[16] // // declare a buffer for writing the OSC packet into
          uint8_t stepper = step; // The step value to send to the OSC client 
          //ESP_LOGI(LINK_TAG, "step : %i", step);

          steps[0].on = true; // Test writing to the array of structs
          //ESP_LOGI(SEQ_TAG, "First step : %i", steps[0].on);                                   

          int maLen = tosc_writeMessage(
            monBuffer, 
            sizeof(monBuffer),
            "/step", // the address // /a second level like /tdlf/step returns an error '122'
            "i",   // the format; 'f':32-bit float, 's':ascii string, 'i':32-bit integer
            stepper
            ); 
      
          // ESP_LOGE(SOCKET_TAG, "maLen : %d; sock :%d", maLen, sock);
          int err = sendto(sock, monBuffer, maLen, 0, (struct sockaddr *)&dest_addr, sizeof(dest_addr));

          if (err < 0) {
            ESP_LOGE(TAG, "Error sending UDP mess: %d", errno);
            printf("err = %i\n", err);
            printf(esp_err_to_name(err));
            printf("\n");
          }

          // if (sock != -1) {
          //     ESP_LOGE(SOCKET_TAG, "Shutting down socket and restarting...");
          //     shutdown(sock_WT32, 0);
          //     close(sock_WT32);
          // }   
            
          // Fin client sockette task, Step sent via OSC 
          // ESP_LOGI(LINK_TAG, "sizeof mtmss : %i", sizeof(mtmss));
    
    // shut off the midi notes that need to be 
    int monTemps = int(esp_timer_get_time()/1000);
    //ESP_LOGI(MIDI_TAG, "Mon Temps, %i", monTemps);
      //for( int i = 0; i < 8; i++ ) {
        //if( MIDI_NOTES_DELAYED_OFF[i] > 0 && MIDI_NOTES_DELAYED_OFF[i] < monTemps ) {

      if( CV_TRIGGER_OFF[0] > 0 && CV_TRIGGER_OFF[0] < monTemps ) {
          gpio_set_level(CV_18, 0); // DAC_D // CV Trigger
          // gpio_set_level(CV_5, 0); // DAC_B // CV Clock Out
          // gpio_set_level(CV_23, 0); // DAC_C // CV Pitch
        }

      if( MIDI_NOTES_DELAYED_OFF[0] > 0 && MIDI_NOTES_DELAYED_OFF[0] < monTemps ) {
        // ESP_LOGI(MIDI_TAG, "Should attempt to turn this off : %i", i);
        //char zedata0[] = {MIDI_NOTE_OFF};
        //uart_write_bytes(UART_NUM_1,zedata0,1); // note off before anything
          
        char zedata1[] = { MIDI_NOTE_ON_CH[1] }; // défini comme midi channel 1 (testing)
        uart_write_bytes(UART_NUM_1, zedata1, 1); //uart_write_bytes(UART_NUM_1, "0x90", 1); // note on channel 0
          char zedata2[] = {MIDI_NOTES[0]}; // tableau de valeurs de notes hexadécimales 
          //char zedata2[] = {MIDI_NOTES[i]}; // tableau de valeurs de notes hexadécimales 
          uart_write_bytes(UART_NUM_1, zedata2, 1); 
          char zedata3[] = {MIDI_NOTE_VEL_OFF};
          uart_write_bytes(UART_NUM_1,zedata3, 1); // velocité à 0
        }
    //} // end midi note off

          for(int i = 0; i<8;i++){ // 8 x 79 = 632 faut faire ça pour chaque valeur de note
           
              myBar = 0; // We're working on 16 steps so myBar is always '0'
              // myBar =  mtmss[i*79+12] + (mtmss[i*79+13])*2; // 0, 1, 2, 3 bars // how many bars for this note?
              
              //ESP_LOGI(LINK_TAG, "myBar : %i", myBar);
              //ESP_LOGI(LINK_TAG, "stepsLength[myBar] : %i", stepsLength[myBar]);
              dubStep = step%stepsLength[myBar]; // modulo 16 // 32 // 48 // 64
              
              //ESP_LOGI(LINK_TAG, "nouveau 'dub'Step : %i", dubStep);

              currStep = (i*79)+dubStep+15; // 15 is where the step info starts
              //ESP_LOGI(LINK_TAG, "currStep : %i", currStep);

              // for(int j = 0; j<79;j++){
              //      ESP_LOGI(LINK_TAG, "mtmss : %i, %i", j, mtmss[j]);
              // }  

              if (mtmss[currStep] == 1){ // send [midi] note out // mute to be implemented // && !muteRecords[i]){ 
                  ESP_LOGI(LINK_TAG, "Check MIDI_NOTE_ON_CH, %i", channel);
                  ESP_LOGI(LINK_TAG, "Check Note on, %i", i);

                if (channel == 10){ // are we playing drumz // solenoids ?
                  char zedata1[] = { MIDI_NOTE_ON_CH[channel] }; // défini comme channel 10(drums), ou channel 1(synth base) pour l'instant mais dois pouvoir changer
                  uart_write_bytes(UART_NUM_1, zedata1, 1); // this function will return after copying all the data to tx ring buffer, UART ISR will then move data from the ring buffer to TX FIFO gradually.
                  char zedata2[] = {zeDrums[i]};      // arriver de 0-8
                  uart_write_bytes(UART_NUM_1, zedata2, 1); // tableau de valeurs de notes hexadécimales 
                  char zedata3[] = { MIDI_NOTE_VEL };
                  uart_write_bytes(UART_NUM_1, zedata3, 1); // vélocité
                  
                  gpio_set_level(CV_18,1); // DAC_D (CV_18) get that note out !    
                  CV_TRIGGER_OFF[0] = int((esp_timer_get_time()/1000)+(myNoteDuration/64)); // set trigger duration
                }

              else if (channel == 5){ // synth, {0x99,0x90};

                char zedata1[] = { MIDI_NOTE_ON_CH[channel] }; // défini comme midi channel channel 0 
                // ESP_LOGI(MIDI_TAG, "MIDI_NOTE_ON_CH, %i", MIDI_NOTE_ON_CH[channel]);
                uart_write_bytes(UART_NUM_1, zedata1, 1); 

                char zedata2[] = {zeDark[i]}; // tableau de valeurs de notes hexadécimales 
                uart_write_bytes(UART_NUM_1, zedata2, 1); 

                char zedata3[] = { MIDI_NOTE_VEL };
                uart_write_bytes(UART_NUM_1, zedata3, 1); // vélocité
                  
                //const auto tempo = state.tempo(); // quelle est la valeur de tempo?
                //myNoteDuration = ((60/(tempo*4))*1000)*noteDuration; // calculate noteDuration as a function of BPM // 60 BPM * 4 steps per beat = 240 steps per minute // 60 seconds / 240 steps = 0,25 secs or 250 milliseconds per step // ((60 seconds / (BPM * 4 steps per beat))*1000 ms)*noteDuration
                // check if there is a value in the array and populate it or just add the note
                //int posArray = step%8;
                //ESP_LOGI(MIDI_TAG, "posArray, %i", posArray);
                //MIDI_NOTES[posArray] = zeDark[i];
                //MIDI_NOTES_DELAYED_OFF[posArray] = int((esp_timer_get_time()/1000)+myNoteDuration); // duration

                MIDI_NOTES[0] = zeDark[i];
                MIDI_NOTES_DELAYED_OFF[0] = int((esp_timer_get_time()/1000)+myNoteDuration); // duration

                ESP_LOGI(MIDI_TAG, "MIDI NOTE, %i", MIDI_NOTES[0]);
                ESP_LOGI(MIDI_TAG, " "); // new line
                // ESP_LOGI(MIDI_TAG, "MIDI TIME, %i", MIDI_NOTES_DELAYED_OFF[0]);

                }

                else{
                  ESP_LOGI(MIDI_TAG, "Midi channel other than 0 or 1"); // new line
                  // ajouter d'autres gammes (scales)
                } 
                
                //char zedata3[] = { MIDI_NOTE_VEL };
                //uart_write_bytes(UART_NUM_1, zedata3, 1); // vélocité
                uart_write_bytes(UART_NUM_1, "0", 1); // ??
              }       
          }
           // ESP_LOGI(LINK_TAG, "");
        }

        if(isPlaying){
          // ESP_LOGI(LINK_TAG, "We should see this step if we're playing : step : %d", step); 
          step++; // might be off to add '1' right away
          }

      }

    } // fin de if curr_beat_time is > prev_beat_time

    prev_beat_time = curr_beat_time;
    portYIELD();
  }

} // fin de tick task
